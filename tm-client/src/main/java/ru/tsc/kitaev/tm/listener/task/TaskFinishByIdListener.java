package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskFinishByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-finish-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish task by id...";
    }

    @Override
    @EventListener(condition = "@taskFinishByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        taskEndpoint.finishTaskById(sessionService.getSession(), id);
    }

}
