package ru.tsc.kitaev.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.Session;

@Repository
public interface SessionRepository extends AbstractRepository<Session> {

}
