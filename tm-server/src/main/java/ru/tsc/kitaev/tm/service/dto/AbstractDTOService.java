package ru.tsc.kitaev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.service.dto.IDTOService;
import ru.tsc.kitaev.tm.dto.AbstractEntityDTO;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.user.UserNotFoundException;
import ru.tsc.kitaev.tm.repository.dto.AbstractDTORepository;

import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractDTOService<E extends AbstractEntityDTO> implements IDTOService<E> {

    @NotNull
    @Autowired
    private AbstractDTORepository<E> repository;

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Nullable
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    public E findById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void removeById(@NotNull final String id) {
        @Nullable final E entity = repository.findById(id).orElse(null);
        if (entity == null) throw new UserNotFoundException();
        repository.delete(entity);
    }

    @Transactional
    public void addAll(@NotNull final List<E> entities) {
        repository.saveAll(entities);
    }

    public int getSize() {
        return (int) repository.count();
    }

}
